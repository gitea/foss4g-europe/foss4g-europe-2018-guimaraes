# FOSS4G-Europe 2018 Guimarães (Portugal) presentations

This repository aims to provide the presentations made during the
[FOSS4G Europe 2018](https://europe.foss4g.org/2018).

They are published with the authorization of their owners.

## Presentations

Are comin soon.

## Contributing

If you want to contribute to add a missing presentation, fixing a typo; please
clone the repository and send your changes with a pull request.

## License

All presentations are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

![Creative Commons BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

See the [LICENSE.md](LICENSE.md) file for details.


